// Fill out your copyright notice in the Description page of Project Settings.


#include "GPAssetManager.h"
#include "AbilitySystemGlobals.h"


UGPAssetManager& UGPAssetManager::Get() 
{
	UGPAssetManager* Singleton = Cast<UGPAssetManager>(GEngine->AssetManager);

	if (Singleton)
	{
		return *Singleton;
	}
	else
	{
		UE_LOG(LogTemp, Fatal, TEXT("Invalid AssetManager in DefaultEngine.ini, must be GPAssetManager!"));
		return *NewObject<UGPAssetManager>();	 // never calls this
	}
}


void UGPAssetManager::StartInitialLoading() 
{
	Super::StartInitialLoading();
	UAbilitySystemGlobals::Get().InitGlobalData();
}


