// Fill out your copyright notice in the Description page of Project Settings.


#include "Player/GPPlayerController.h"
#include "AbilitySystemComponent.h"
#include "Player/GPPlayerState.h"
#include "UI/GPDamageTextWidgetComponent.h"

AGPPlayerController::AGPPlayerController()
{
	bShowMouseCursor = true;
	DefaultMouseCursor = EMouseCursor::Crosshairs;
}

void AGPPlayerController::ShowDamageNumber_Implementation(float DamageAmount, AGPCharacterBase* TargetCharacter)
{
	if (TargetCharacter)
	{
		UGPDamageTextWidgetComponent* DamageText = NewObject<UGPDamageTextWidgetComponent>(TargetCharacter, DamageNumberClass);
		DamageText->RegisterComponent();
		DamageText->AttachToComponent(TargetCharacter->GetRootComponent(), FAttachmentTransformRules::KeepRelativeTransform);
		DamageText->SetDamageText(DamageAmount);
	}
}

bool AGPPlayerController::ShowDamageNumber_Validate(float DamageAmount, AGPCharacterBase* TargetCharacter)
{
	return true;
}


void AGPPlayerController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	AGPPlayerState* PS = GetPlayerState<AGPPlayerState>();
	if (PS)
	{
		// Init ASC with PS (Owner) and our new Pawn (AvatarActor)
		PS->GetAbilitySystemComponent()->InitAbilityActorInfo(PS, InPawn);
	}
}

void AGPPlayerController::OnRep_PlayerState()
{
	Super::OnRep_PlayerState();

	// For edge cases where the PlayerState is repped before the MainCharacter is possessed.
	// CreateHUD();
}
