// Fill out your copyright notice in the Description page of Project Settings.


#include "Player/GPPlayerState.h"
#include "Abilities/AttributeSets/GPAttributeSetBase.h"
#include "Abilities/GPAbilitySystemComponent.h"
#include "Characters/GPMainCharacter.h"
#include "Player/GPPlayerController.h"
#include "UI/GPFloatingStatusBarWidget.h"

AGPPlayerState::AGPPlayerState()
{
	// Create ability system component, and set it to be explicitly replicated
	AbilitySystemComponent = CreateDefaultSubobject<UGPAbilitySystemComponent>(TEXT("AbilitySystemComponent"));
	AbilitySystemComponent->SetIsReplicated(true);

	// Mixed mode means we only are replicated the GEs to ourself, not the GEs to simulated proxies. If another GDPlayerState (Hero) receives a GE,
	// we won't be told about it by the Server. Attributes, GameplayTags, and GameplayCues will still replicate to us.
	AbilitySystemComponent->SetReplicationMode(EGameplayEffectReplicationMode::Mixed);

	// Create the attribute set, this replicates by default
	// Adding it as a subobject of the owning actor of an AbilitySystemComponent
	// automatically registers the AttributeSet with the AbilitySystemComponent
	AttributeSetBase = CreateDefaultSubobject<UGPAttributeSetBase>(TEXT("AttributeSetBase"));

	// Set PlayerState's NetUpdateFrequency to the same as the Character.
	// Default is very low for PlayerStates and introduces perceived lag in the ability system.
	// 100 is probably way too high for a shipping game, you can adjust to fit your needs.
	NetUpdateFrequency = 100.0f;

	// Cache tags
	DeadTag = FGameplayTag::RequestGameplayTag(FName("State.Dead"));
}

UAbilitySystemComponent * AGPPlayerState::GetAbilitySystemComponent() const
{
	return AbilitySystemComponent;
}

UGPAttributeSetBase * AGPPlayerState::GetAttributeSetBase() const
{
	return AttributeSetBase;
}

bool AGPPlayerState::IsAlive() const
{
	return GetHealth() > 0.0f;
}

float AGPPlayerState::GetHealth() const
{
	return AttributeSetBase->GetHealth();
}

float AGPPlayerState::GetMaxHealth() const
{
	return AttributeSetBase->GetMaxHealth();
}

float AGPPlayerState::GetActionPoints() const
{
	return AttributeSetBase->GetActionPoints();
}

float AGPPlayerState::GetMaxActionPoints() const
{
	return AttributeSetBase->GetMaxActionPoints();
}

int32 AGPPlayerState::GetCharacterLevel() const
{
	return AttributeSetBase->GetCharacterLevel();
}

void AGPPlayerState::BeginPlay()
{
	Super::BeginPlay();

	// Attribute change callbacks
	HealthChangedDelegateHandle = AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(AttributeSetBase->GetHealthAttribute()).AddUObject(this, &AGPPlayerState::HealthChanged);
	MaxHealthChangedDelegateHandle = AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(AttributeSetBase->GetMaxHealthAttribute()).AddUObject(this, &AGPPlayerState::MaxHealthChanged);
	ActionPointsChangedDelegateHandle = AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(AttributeSetBase->GetActionPointsAttribute()).AddUObject(this, &AGPPlayerState::ActionPointsChanged);
	MaxActionPointsChangedDelegateHandle = AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(AttributeSetBase->GetMaxActionPointsAttribute()).AddUObject(this, &AGPPlayerState::MaxActionPointsChanged);
	CharacterLevelChangedDelegateHandle = AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(AttributeSetBase->GetCharacterLevelAttribute()).AddUObject(this, &AGPPlayerState::CharacterLevelChanged);

	// tag change callbacks
	AbilitySystemComponent->RegisterGameplayTagEvent(FGameplayTag::RequestGameplayTag(FName("State.Debuff.Stun")), EGameplayTagEventType::NewOrRemoved).AddUObject(this, &AGPPlayerState::StunTagChanged);
}

void AGPPlayerState::HealthChanged(const FOnAttributeChangeData & Data)
{
	float Health = Data.NewValue;

	// Update floating status bar
	AGPMainCharacter* Hero = Cast<AGPMainCharacter>(GetPawn());
	if (Hero)
	{
		UGPFloatingStatusBarWidget* HeroFloatingStatusBar = Hero->GetFloatingStatusBar();
		if (HeroFloatingStatusBar)
		{
			HeroFloatingStatusBar->SetHealthPercentage(Health / GetMaxHealth());
		}
	}

	// Update the HUD
	// Handled in the UI itself using the AsyncTaskAttributeChanged node as an example how to do it in Blueprint

	// If the player died, handle death
	if (!IsAlive() && !AbilitySystemComponent->HasMatchingGameplayTag(DeadTag))
	{
		if (Hero)
		{
			Hero->Die();
		}
	}
}

void AGPPlayerState::MaxHealthChanged(const FOnAttributeChangeData & Data)
{
	float MaxHealth = Data.NewValue;

	// Update floating status bar
	AGPMainCharacter* Hero = Cast<AGPMainCharacter>(GetPawn());
	if (Hero)
	{
		UGPFloatingStatusBarWidget* HeroFloatingStatusBar = Hero->GetFloatingStatusBar();
		if (HeroFloatingStatusBar)
		{
			HeroFloatingStatusBar->SetHealthPercentage(GetHealth() / MaxHealth);
		}
	}

	// Update the HUD
	AGPPlayerController* PC = Cast<AGPPlayerController>(GetOwner());
	if (PC)
	{
		// UGDHUDWidget* HUD = PC->GetHUD();
		// if (HUD)
		// {
		// 	HUD->SetMaxHealth(MaxHealth);
		// }
	}
}

void AGPPlayerState::ActionPointsChanged(const FOnAttributeChangeData & Data)
{
	float ActionPoints = Data.NewValue;

	// Update floating status bar
	AGPMainCharacter* Hero = Cast<AGPMainCharacter>(GetPawn());
	if (Hero)
	{
		
	}

	// Update the HUD
	// Handled in the UI itself using the AsyncTaskAttributeChanged node as an example how to do it in Blueprint
}

void AGPPlayerState::MaxActionPointsChanged(const FOnAttributeChangeData & Data)
{
	float MaxActionPoints = Data.NewValue;

	// Update floating status bar
	AGPMainCharacter* Hero = Cast<AGPMainCharacter>(GetPawn());
	if (Hero)
	{
		
	}

	// Update the HUD
	// Handled in the UI itself using the AsyncTaskAttributeChanged node as an example how to do it in Blueprint
}

void AGPPlayerState::CharacterLevelChanged(const FOnAttributeChangeData & Data)
{
	float CharacterLevel = Data.NewValue;

	// Update the HUD
	AGPPlayerController* PC = Cast<AGPPlayerController>(GetOwner());
	if (PC)
	{
		// UGDHUDWidget* HUD = PC->GetHUD();
		// if (HUD)
		// {
		// 	HUD->SetHeroLevel(CharacterLevel);
		// }
	}
}

void AGPPlayerState::StunTagChanged(const FGameplayTag CallbackTag, int32 NewCount)
{
	if (NewCount > 0)
	{
		FGameplayTagContainer AbilityTagsToCancel;
		AbilityTagsToCancel.AddTag(FGameplayTag::RequestGameplayTag(FName("Ability")));

		FGameplayTagContainer AbilityTagsToIgnore;
		AbilityTagsToIgnore.AddTag(FGameplayTag::RequestGameplayTag(FName("Ability.NotCanceledByStun")));

		AbilitySystemComponent->CancelAbilities(&AbilityTagsToCancel, &AbilityTagsToIgnore);
	}
}
