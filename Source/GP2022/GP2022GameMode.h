// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "GP2022GameMode.generated.h"

/**
 * 
 */
UCLASS(minimalapi)
class AGP2022GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AGP2022GameMode();

	void HeroDied(AController* Controller);

protected:
	float RespawnDelay;

	TSubclassOf<class AGPMainCharacter> HeroClass;

	AActor* EnemySpawnPoint;

	virtual void BeginPlay() override;

	void RespawnHero(AController* Controller);
};
