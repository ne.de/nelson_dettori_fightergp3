// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

UENUM(BlueprintType)
enum class EGPAbilityInputID : uint8
{
	// 0 None
	None			UMETA(DisplayName = "None"),
	// 1 Confirm
	Confirm			UMETA(DisplayName = "Confirm"),
	// 2 Cancel
	Cancel			UMETA(DisplayName = "Cancel"),
	// 3
	Ability1		UMETA(DisplayName = "Ability1"),
	// 4
	Ability2		UMETA(DisplayName = "Ability2"),
	// 5
	Ability3		UMETA(DisplayName = "Ability3"),
	// 6
	Ability4		UMETA(DisplayName = "Ability4")/*,
	// 7
	Ability5		UMETA(DisplayName = "Ability5")*/
};
