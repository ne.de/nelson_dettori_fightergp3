// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Characters/GPCharacterBase.h"
#include "GameplayEffectTypes.h"
#include "GPAICharacter.generated.h"

/**
 * 
 */
UCLASS()
class GP2022_API AGPAICharacter : public AGPCharacterBase
{
	GENERATED_BODY()

public:
	AGPAICharacter(const class FObjectInitializer& ObjectInitializer);

	// Actual hard pointer to AbilitySystemComponent
	UPROPERTY()
	class UGPAbilitySystemComponent* HardRefAbilitySystemComponent;

	// Actual hard pointer to AttributeSetBase
	UPROPERTY()
	class UGPAttributeSetBase* HardRefAttributeSetBase;
	
	virtual void BeginPlay() override;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "GAS|UI")
	TSubclassOf<class UGPFloatingStatusBarWidget> UIFloatingStatusBarClass;

	UPROPERTY()
	class UGPFloatingStatusBarWidget* UIFloatingStatusBar;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "GAS|UI")
	class UWidgetComponent* UIFloatingStatusBarComponent;

	FDelegateHandle HealthChangedDelegateHandle;

	// Attribute changed callbacks
	virtual void HealthChanged(const FOnAttributeChangeData& Data);

	// Tag change callbacks
	virtual void StunTagChanged(const FGameplayTag CallbackTag, int32 NewCount);
};
