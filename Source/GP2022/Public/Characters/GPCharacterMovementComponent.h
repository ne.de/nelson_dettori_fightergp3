// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GPCharacterMovementComponent.generated.h"

/**
 * 
 */
UCLASS()
class GP2022_API UGPCharacterMovementComponent : public UCharacterMovementComponent
{
	GENERATED_BODY()
	
};
