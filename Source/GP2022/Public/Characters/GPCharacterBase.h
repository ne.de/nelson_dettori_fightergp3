// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "AbilitySystemInterface.h"
#include "GameplayTagContainer.h"
#include "GP2022/GP2022.h"
#include "GPCharacterBase.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FCharacterDiedDelegate, AGPCharacterBase*, Character);

UCLASS()
class GP2022_API AGPCharacterBase : public ACharacter, public IAbilitySystemInterface
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AGPCharacterBase(const class FObjectInitializer& ObjectInitializer);

	UPROPERTY(BlueprintAssignable, Category = "GAS|GPCharacter")
	FCharacterDiedDelegate OnCharacterDied;
	
	// Implement IAbilitySystemInterface
	virtual class UAbilitySystemComponent* GetAbilitySystemComponent() const override;

	UFUNCTION(BlueprintCallable, Category = "GAS|Character")
	bool IsAlive() const;
	
	UFUNCTION(BlueprintCallable, Category = "GAS|Character")
	int32 GetAbilityLevel(EGPAbilityInputID AbilityID) const;

	// Removes all CharacterAbilities. Can only be called by the Server. Removing on the Server will remove from Client too.
	virtual void RemoveCharacterAbilities();

	
	/**
	* Getters for attributes from GPAttributeSetBase
	**/
	
	UFUNCTION(BlueprintCallable, Category = "GAS|Character|Attributes")
	int32 GetCharacterLevel() const;

	UFUNCTION(BlueprintCallable, Category = "GAS|Character|Attributes")
	float GetHealth() const;

	UFUNCTION(BlueprintCallable, Category = "GAS|Character|Attributes")
	float GetMaxHealth() const;

	UFUNCTION(BlueprintCallable, Category = "GAS|Character|Attributes")
	float GetActionPoints() const;

	UFUNCTION(BlueprintCallable, Category = "GAS|Character|Attributes")
	float GetMaxActionPoints() const;
	
	virtual void Die();

	UFUNCTION(BlueprintCallable, Category = "GAS|GPCharacter")
	virtual void FinishDying();
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Instead of TWeakObjectPtrs, you could just have UPROPERTY() hard references or no references at all and just call
	// GetAbilitySystem() and make a GetAttributeSetBase() that can read from the PlayerState or from child classes.
	// Just make sure you test if the pointer is valid before using.
	// I opted for TWeakObjectPtrs because I didn't want a shared hard reference here and I didn't want an extra function call of getting
	// the ASC/AttributeSet from the PlayerState or child classes every time I referenced them in this base class.

	TWeakObjectPtr<class UGPAbilitySystemComponent> AbilitySystemComponent;
	TWeakObjectPtr<class UGPAttributeSetBase> AttributeSetBase;

	//base tags
	FGameplayTag DeadTag;
	FGameplayTag EffectRemoveOnDeathTag;
	
	// Death Animation
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "GAS|Animation")
	UAnimMontage* DeathMontage;
	
	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "GAS|Abilities")
	TArray<TSubclassOf<class UGPGameplayAbility>> CharacterAbilities;

	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "GAS|Abilities")
	TSubclassOf<class UGameplayEffect> DefaultAttributes;

	virtual void AddCharacterAbilities();

	virtual void InitializeAttributes();

	//add startupeffects

	//setters for base value attributes (on respawn?)
	/**
	* Setters for Attributes. Only use these in special cases like Respawning, otherwise use a GE to change Attributes.
	* These change the Attribute's Base Value.
	*/

	virtual void SetHealth(float Health);
	virtual void SetActionPoints(float ActionPoints);
};
