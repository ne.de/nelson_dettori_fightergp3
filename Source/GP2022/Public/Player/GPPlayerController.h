// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "Characters/GPCharacterBase.h"
#include "GPPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class GP2022_API AGPPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	//create HUD
	AGPPlayerController();

	UPROPERTY(EditAnywhere, Category = "GAS|UI")
	TSubclassOf<class UGPDamageTextWidgetComponent> DamageNumberClass;

	UFUNCTION(Client, Reliable, WithValidation)
	void ShowDamageNumber(float DamageAmount, AGPCharacterBase* TargetCharacter);
	void ShowDamageNumber_Implementation(float DamageAmount, AGPCharacterBase* TargetCharacter);
	bool ShowDamageNumber_Validate(float DamageAmount, AGPCharacterBase* TargetCharacter);

protected:
	virtual void OnPossess(APawn* InPawn) override;
	
	//virtual void onrepPS?
	virtual void OnRep_PlayerState() override;	
};
