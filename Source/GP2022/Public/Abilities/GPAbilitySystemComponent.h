// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AbilitySystemComponent.h"
#include "GPAbilitySystemComponent.generated.h"

//DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FReceivedDamageDelegate, UGPAbilitySystemComponent*, SourceASC, float, UnmitigatedDamage, float, MitigatedDamage);

/**
 * 
 */
UCLASS()
class GP2022_API UGPAbilitySystemComponent : public UAbilitySystemComponent
{
	GENERATED_BODY()

public:
	bool CharacterAbilitiesGiven = false;

	//FReceivedDamageDelegate ReceivedDamage;

	//**Called from GDDamageExecCalculation. Broadcasts on ReceivedDamage whenever this ASC receives damage.
	//virtual void ReceiveDamage(UGPAbilitySystemComponent* SourceASC, float UnmitigatedDamage, float MitigatedDamage);
};
