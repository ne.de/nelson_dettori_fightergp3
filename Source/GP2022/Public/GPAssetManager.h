// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/AssetManager.h"
#include "GPAssetManager.generated.h"

/**
 * 
 */
UCLASS()
class GP2022_API UGPAssetManager : public UAssetManager
{
	GENERATED_BODY()
	
public:
	static UGPAssetManager& Get();

	/** Starts initial load, gets called from InitializeObjectReferences */
	virtual void StartInitialLoading() override;
};
